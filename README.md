REGIST.IO - Docker backup for mysql on S3 (and compatible)
=========================================================

This docker image allow to do backup the complete database server from a container regularly and store the data on S3.

The backup are done following a cron like period defined by an environment variable.

The daemon is creating in each cycle:
* `latest_backup.gz` how is the latest backup (override the latest if exist).
* `backup_monthly_%s.gz` with %s is the current month.
* `backup__daily_%s.gz` with %s is the day number for current day.

The result of this pattern is that :
* we have always the latest backup in a predictable place `latest_backup.gz`
* we have one backup for every day of the past ~30 days and we have one monthly backup for the past 12 months.
* we don't need to clean/remove the older backups as older backup are automatically override.

This project is hosted on [Gitlab](https://gitlab.com/regist.io/docker-mysql-s3-backup).

### Example:
```bash
docker run \
    --rm \
    --name test-backup \
    --link test-mysql:db \
    -e DB_USER=user \
    -e DB_PASSWORD=password \
    -e DB_DUMP_CRON='* * * * * */12 0 0' \
    -e S3_TARGET=rancher/backup \
    -e S3_BUCKET=regist.io \
    -e AWS_KEY_ID= xxxxxxxxxxx\
    -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxx \
    -e AWS_DEFAULT_REGION=eu-west-1 \
    registio/mysql-s3-backup:latest
```

Where **--link** link to the database container.

### Environment variable:

| Env name              | Required | Default Value | Description                                                     |
|-----------------------|----------|---------------|-----------------------------------------------------------------|
| DB_USER               | Yes      | -             | Username to access the database.                                |
| DB_PASS               | Yes      | -             | Password to access the database.                                |
| DB_DUMP_CRON          | Yes      | -             | Frequency of the backup using cron-like expression*             |
| S3_BUCKET             | Yes      | -             | Bucket name to store the backup.                                |
| S3_TARGET             | Yes      | -             | Relative Path to the S3 bucket (ej: backup/mysql ).             |
| AWS_KEY_ID            | Yes      | -             | AWS Key ID.                                                     |
| AWS_SECRET_ACCESS_KEY | Yes      | -             | AWS Secret Access Key.                                          |
| AWS_DEFAULT_REGION    | Yes      | -             | Region in which the bucket resides.                             |
| RESTORE_BACKUP        | No       | False         | Boolean to define if the last backup will be restored on start. |
| PREFFIX               | No       | mysql         | Prefix used to name backup files.                               |
| TIMEZONE              | No       | Europe/Paris  | Time Zone used for the cron job.                                |

**Cron-like:** The cron like expression has to be in the following format:

| year month  | day week | day of the week | hour | minute | second |
|-------------|----------|-----------------|------|--------|--------|
| *           | *        | *               | *    | *      | *      |
| *           | *        | *               | 22   | 0      | 0      |
