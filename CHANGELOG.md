CHANGELOG
=========

## Version 0.3.0
_18-11-2016_
* Add proper logger and fix some bugs.
* Add loop and wait for the restore script.

## Version 0.2.0
_03-11-2016_
* Change the pattern for the names of the backup files (see readme file.)

## Version 0.1.0
_12-08-2016_
* First Version.
