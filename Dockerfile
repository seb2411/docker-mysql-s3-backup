FROM alpine:3.4
MAINTAINER lemarinel.s@gmail.com

# install the necessary client
RUN apk add --update mysql-client python py-pip && \
    rm -rf /var/cache/apk/* && \
    pip install boto3 apscheduler

# install the entrypoint
COPY script/backup.py /

# start
CMD python /backup.py
