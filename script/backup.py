'''
    Database backup script.
'''
import time
import os
import boto3
import botocore
import logging

from apscheduler.schedulers.blocking import BlockingScheduler

pid = "/tmp/docker-mysql-s3-backup.pid"


class Backup:

    # Database credentials.
    dbUser = ''
    dbPassword = ''
    dbServer = 'db'

    # Cron job expression
    dbDumpCron = ''

    # Backup target.
    s3Target = ''
    s3Bucket = ''

    # S3 credentials
    awsKeyId = ''
    awsSecretAccessKey = ''
    awsDefaultRegion = ''

    # Restore strategy
    restoreBackup = False

    # Domain used to name the backup files.
    preffix = 'mysql'

    # Temporary directories
    tempBackupDir = "/tmp/backups"
    tempRestoreDir = "/tmp/restorefile"

    # Timezone
    timezone = "Europe/Paris"

    # S3 Connection
    s3 = False

    '''
        Get the environment variables and set the defaults values if necessary
    '''
    def __init__(self):
        # Database credentials.
        self.setVars('DB_USER')
        self.setVars('DB_PASSWORD')
        self.setVars('DB_DUMP_CRON')

        # S3 credentials
        self.setVars('S3_TARGET')
        self.setVars('S3_BUCKET')

        # AWS Credentials
        self.setVars('AWS_KEY_ID')
        self.setVars('AWS_SECRET_ACCESS_KEY')
        self.setVars('AWS_DEFAULT_REGION')

        # Restore backup on launch
        self.setVars('RESTORE_BACKUP', False)

        # Restore backup on launch
        self.setVars('PREFFIX', False)
        self.setVars('TIMEZONE', False)

        self.s3 = boto3.client(
            's3',
            aws_access_key_id=self.awsKeyId,
            aws_secret_access_key=self.awsSecretAccessKey,
            region_name=self.awsDefaultRegion
            )

        # Set logger
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

    '''
        Check if the given environment variable exist
        and raise a an error if not.
    '''
    def setVars(self, name, required=True):
        if os.getenv(name):
            setattr(self, self.envNameToVarName(name), os.getenv(name))
        else:
            if required:
                raise NameError('%s Environment variable missing.' % (name))

    '''
        Converting the environment variable name to the Python varialbe name.
    '''
    def envNameToVarName(self, name):
        varName = ''
        name = name.lower()
        strings = name.split('_')
        count = 1
        for string in strings:
            if count > 1:
                varName = "%s%s" % (varName, string.capitalize())
            else:
                varName = "%s%s" % (varName, string)
            count += 1

        return varName

    '''
        Backup the database to S3.
    '''
    def backup(self):

        if not os.path.isdir(self.tempBackupDir):
            os.system('mkdir %s' % (self.tempBackupDir))

        filenameMonthly = self.generateFileName('monthly')
        filenameDaily = self.generateFileName('daily')
        filenameTemp = 'tempBackup.gz'

        # Dump the database.
        dbBackupRequest = "mysqldump -A -h %s -u%s -p%s | gzip > %s/%s" % (
            self.dbServer,
            self.dbUser,
            self.dbPassword,
            self.tempBackupDir,
            filenameTemp
        )
        logging.info(dbBackupRequest)
        os.system(dbBackupRequest)

        # Save dump on S3
        # Save dump monthly.
        self.s3.upload_file("%s/%s" % (self.tempBackupDir, filenameTemp), self.s3Bucket, "%s/%s" % (self.s3Target, filenameMonthly))
        # Save dump daily.
        self.s3.upload_file("%s/%s" % (self.tempBackupDir, filenameTemp), self.s3Bucket, "%s/%s" % (self.s3Target, filenameDaily))
        # Save dump as latest backup.
        self.s3.upload_file("%s/%s" % (self.tempBackupDir, filenameTemp), self.s3Bucket, "%s/%s" % (self.s3Target, "latest_backup.gz"))

    '''
        Restore the database from S3
    '''
    def restore(self):
        logging.info("Restore from the bucket %s and target %s" % (self.s3Bucket, self.s3Target))
        filename = 'latest_backup.gz';

        if (self.fileExist("%s/%s" % (self.s3Target, filename))):

            if not os.path.isdir(self.tempRestoreDir):
                os.system('mkdir %s' % (self.tempRestoreDir))

            self.s3.download_file(self.s3Bucket, "%s/%s" % (self.s3Target, filename), "%s/%s" % (self.tempRestoreDir, filename))

            # Dump the database.
            restoreStatus = 1
            restoreTry = 0
            while restoreStatus != 0 and restoreTry < 20:
                if restoreTry > 0:
                    time.sleep(2)
                dbRestoreRequest = "gunzip < %s/%s | mysql -h %s -u %s -p%s" % (self.tempRestoreDir, filename, self.dbServer, self.dbUser, self.dbPassword)
                restoreStatus = os.system(dbRestoreRequest)
                restoreTry += 1
                if restoreStatus == 0:
                    logging.info("Restoring...Connection successful.")

            rmTmpFile = "/bin/rm -f %s/%s" % (self.tempBackupDir, filename)
            os.system(rmTmpFile)
        else:
            logging.info("No File for restoring the database.")

    def explodeCron(self, expr):
        return expr.split(' ')

    '''
        Indicate if it's time to clean backup files
    '''
    def isTimeForCleaning(self):
        return False

    '''
        Indicate if it's time to clean backup files
    '''
    def generateFileName(self, typeName):

        if typeName == 'monthly':
            filename = self.preffix + '_backup_monthly_%s.gz' % (time.strftime("%m"))
        elif typeName == 'daily':
            filename = self.preffix + '_backup_daily_%s.gz' % (time.strftime("%d"))
        else:
            filename = self.preffix + '_backup_%s.gz' % (time.strftime("%Y%m%d%H%M%S"))

        return filename

    '''
        Check if the remote file exist.
    '''
    def fileExist(self, filename):
        exists = False

        try:
            self.s3.head_object(Bucket=self.s3Bucket, Key=filename)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                exists = False
            else:
                raise e
        else:
            exists = True

        return exists

    '''
        Execute deamon.
    '''
    def main(self):

        if self.restoreBackup:
            logging.info("Restoring database if necessary.")
            self.restore()

        logging.info("Process Cron expression.")
        cron = self.explodeCron(self.dbDumpCron)

        # Initialize schedulers
        logging.info("Prepare scheduler.")
        scheduler = BlockingScheduler(timezone=self.timezone)
        scheduler.add_job(
            self.backup,
            'cron',
            year=cron[0],
            month=cron[1],
            day=cron[2],
            week=cron[3],
            day_of_week=cron[4],
            hour=cron[5],
            minute=cron[6],
            second=cron[7]
        )

        logging.info("Start Scheduler.")
        scheduler.start()


bakcupInstance = Backup()
bakcupInstance.main()
